'use strict';

var NameApi = require('../api/name');


var Name = {
   register: function (req, res, next) {

      var options = {
         name: req.body.name
     };

     NameApi.register(req, options, function (err, name) {
         if (err) {
            err.status = 417;
            return next(err);
        }
        req.data = name;
        next();
    });
 },

 getName: function (req, res, next) {
  var options = { id: req.params.id };

  NameApi.getName(options, function (err, name) {
     if (err || !name) {
        err = err || new Error("Name Not Found");
        err.status = 417;
        return next(err);
    }
    req.data = name;
    next();
});
},

getRandomName: function (req, res, next) {

  var options = {};

  NameApi.getRandomName(req, options, function (err, name) {
     if (err || !name) {
        err = err || new Error('No names were found');
        err.status = 417;
        return next(err);
    }
    req.data = name;
    next();
});
},

getAllNames: function (req, res, next) {
  var options = {};

  NameApi.getAllNames(req, options, function (err, names) {
     if (err || !names) {
        err = err || new Error('No names were found');
        err.status = 417;
        return next(err);
    }
    req.data = names;
    next();
});
},

updateName: function (req, res, next) {
  var options = {
     id: req.params.id,
     name: req.body.name,
 };

 NameApi.updateName(options, function (err, name) {
     if (err) {
        err.status = 417;
        return next(err);
    }
    req.data = name;
    next();
});
},

deleteName: function (req, res, next) {
  var options = {
     id: req.params.id,
 };

 NameApi.deleteName(options, function (err) {
     if (err) {
        err.status = 417;
        return next(err);
    }
    next();
});
},

sendJSON: function (req, res, next) {
  res.json(req.data);
}
};

module.exports = Name;

(function () {
   if (require.main == module) {
   }
}());