const express = require('express');
const router = express.Router();

const Name = require('./name');


//Event CRUD A
router.post('/name', Name.register, Name.sendJSON);
router.put('/name/:id', Name.updateName, Name.sendJSON);
router.get('/name/:id', Name.getName, Name.sendJSON);
router.get('/names', Name.getAllNames, Name.sendJSON);
router.get('/name', Name.getRandomName, Name.sendJSON);
router.delete('/name/:id', Name.deleteName, Name.sendJSON);

module.exports = router;


