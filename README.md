# Prerequisites #
* Make sure you have ng-cli 1.0.0 installed // npm install @angular/cli@1.0.0 -g

### TO run the app ###

* npm start
* ng serve 
* Go to http://localhost:4200
