
const NameModel = require('../models/name');


let Name = {

    register: function (req, options, cb) {

        let newName = options.name;
        let Options = {
         name: newName
     };

     let name = new NameModel(Options);

     name.save(function (err, name) {
        if (err) return cb(err);
        return cb(null, name);

    })
 },

 getName: function (options, cb) {
    var nameId = options.id;

    if (nameId == undefined) {
        var err = new Error("Name Not Found");
        return cb(err);
    }

    var Options = { _id: nameId };

    NameModel.find(Options, function (err, name) {
        if (err || !name || !name.length) {
            err = err || new Error("Name Not Found");
            return cb(err);
        }
        name = name[0];
        return cb(null, name);
    });
},

getRandomName: function(req, options, cb){
        NameModel.find({}, function (err, names) {
        if (err || !names || !names.length) {
            err = err || new Error("Could not get names list");
            return cb(err);
        }


        var name = names[Math.floor(Math.random()*names.length)];
        return cb(null, name);
})

},

getAllNames: function (req, options, cb) {

    NameModel.find({}, function (err, names) {
        if (err || !names || !names.length) {
            err = err || new Error("Could not get names list");
            return cb(err);
        }

        namesList = [];
        for (let i=0; i<names.length; i++){
            namesList.push(names[i]);
        }
        return cb(null, namesList);
    });
},

updateName: function (options, cb) {

    Name.getName(options, function (err, name) {
        if (err || !name) {
            err = err || new Error("Name Not Found");
            return cb(err);
        }

        var updatedname = options.name;

        if ( name != undefined ) {
            name.name = updatedname;
        }

        name.save(function (err) {
            if (err) {
                return cb(err);
            }
            return cb(null, name);
        });
    });
},

deleteName: function (options, cb) {

    Name.getName(options, function (err, name) {

        if (err || !name) {
            err = err || new Error("Name Not Found");
            return cb(err);
        }

        name.remove(function (err) {
            if (err) {
                return cb(err);
            }
            return cb();
        });
    });
}
};

module.exports = Name;