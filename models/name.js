const mongoose = require('mongoose');

const config = require('../config/db');

// User Schema
const NameSchema = mongoose.Schema({
  name: {
    type: String
  }
});

const Name = module.exports = mongoose.model('Name', NameSchema);

module.exports.getNameById = function(id, callback){
  User.findById(id, callback);
}

module.exports.addName = function(newName, callback){
    newName.save(callback);
}
